package com.example.user.ejerciciovistas;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Rodrigo Zea on 2/28/2018.
 */

public interface ICountryDataSource {
    Flowable<List<Country>> getAll();
    void insertCountry(Country... countries);
    void updateCountry(Country... countries);
    void deleteCountry(Country country);
    void deleteAllCountries();
}
