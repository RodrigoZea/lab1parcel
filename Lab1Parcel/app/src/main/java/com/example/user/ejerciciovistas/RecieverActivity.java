package com.example.user.ejerciciovistas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;


/**
 * Created by Rodrigo Zea on 2/22/2018.
 */

public class RecieverActivity extends AppCompatActivity{

    public static final String SELECTED_ITEM = "com.jwhh.jim.notekeeper.NOTE_INFO";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.receiver_selected_layout);

        Intent intent = getIntent();
        modelString itemToRecieve = intent.getParcelableExtra(SELECTED_ITEM);

        String nombre = itemToRecieve.getNombre();
        String prefix = itemToRecieve.getPrefijo();
        String habs = itemToRecieve.getHabitantes();

        TextView txtNom = findViewById(R.id.txtNom);
        txtNom.setText(nombre);
        TextView txtPos = findViewById(R.id.txtPos);
        txtPos.setText(prefix);
        TextView txtHab = findViewById(R.id.txtHab);
        txtHab.setText(habs);

    }


}
