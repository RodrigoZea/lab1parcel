package com.example.user.ejerciciovistas;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Rodrigo Zea on 2/28/2018.
 */

public class CountryDataSource implements ICountryDataSource {
    private CountryDao countryDao;
    private static CountryDataSource dbInstance;

    public CountryDataSource(CountryDao countryDao){
        this.countryDao = countryDao;
    }

    public static CountryDataSource getInstance(CountryDao countryDao){
        if(dbInstance == null) {
            dbInstance = new CountryDataSource(countryDao);
        }
        return dbInstance;
    }

    @Override
    public Flowable<List<Country>> getAll() {
        return countryDao.getAll();
    }

    @Override
    public void insertCountry(Country... countries) {
        countryDao.insertCountry(countries);
    }

    @Override
    public void updateCountry(Country... countries) {
        countryDao.updateCountry(countries);
    }

    @Override
    public void deleteCountry(Country country) {
        countryDao.deleteCountry(country);
    }

    @Override
    public void deleteAllCountries() {
        countryDao.deleteAllCountries();
    }
}
