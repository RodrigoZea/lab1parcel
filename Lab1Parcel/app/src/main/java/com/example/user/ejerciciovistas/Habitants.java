package com.example.user.ejerciciovistas;

import android.arch.persistence.room.ColumnInfo;

/**
 * Created by USER on 2/28/2018.
 */

public class Habitants {
    @ColumnInfo(name = "hab_cant")
    private int cant;

    @ColumnInfo(name = "hab_prefix")
    private String prefix;

    public Habitants(int cant, String prefix) {
        this.cant = cant;
        this.prefix = prefix;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
