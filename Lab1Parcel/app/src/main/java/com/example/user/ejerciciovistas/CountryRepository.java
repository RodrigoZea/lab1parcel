package com.example.user.ejerciciovistas;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Rodrigo Zea on 2/28/2018.
 */

public class CountryRepository implements ICountryDataSource {

    private ICountryDataSource mLocalDataSource;
    private static CountryRepository dbInstance;

    public CountryRepository(ICountryDataSource mLocalDataSource){
        this.mLocalDataSource = mLocalDataSource;
    }

    public static CountryRepository getInstance(ICountryDataSource mLocalDataSource){
        if(dbInstance == null){
            dbInstance = new CountryRepository(mLocalDataSource);
        }
        return dbInstance;
    }

    @Override
    public Flowable<List<Country>> getAll() {
        return mLocalDataSource.getAll();
    }

    @Override
    public void insertCountry(Country... countries) {
        mLocalDataSource.insertCountry(countries);
    }

    @Override
    public void updateCountry(Country... countries) {
        mLocalDataSource.updateCountry(countries);
    }

    @Override
    public void deleteCountry(Country country) {
        mLocalDataSource.deleteCountry(country);
    }

    @Override
    public void deleteAllCountries() {
        mLocalDataSource.deleteAllCountries();
    }
}
