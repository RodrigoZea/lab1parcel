package com.example.user.ejerciciovistas;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by USER on 2/28/2018.
 */

@Dao
public interface CountryDao {
    @Query("SELECT * FROM countries WHERE id=:countryId")
    Flowable<Country> getCountryById(int countryId);

    @Query("SELECT * FROM countries")
    Flowable<List<Country>> getAll();

    @Insert
    void insertCountry(Country... countries);

    @Update
    void updateCountry(Country... countries);

    @Delete
    void deleteCountry(Country country);

    @Query("DELETE FROM countries")
    void deleteAllCountries();
}
