package com.example.user.ejerciciovistas;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by USER on 2/28/2018.
 */

@Entity(tableName = "countries")
public class Country{

    @NonNull
    @PrimaryKey(autoGenerate = true)

    @ColumnInfo(name="id")
    private int id;

    @ColumnInfo(name="name")
    private String name;

    @Embedded
    private Habitants hab;

    public Country(){

    }

    @Ignore
    public Country(@NonNull int id, String name, Habitants hab) {
        this.id = id;
        this.name = name;
        this.hab = hab;
    }

    @Override
    public String toString() {
        return name;
    }

    @Ignore
    public Country(String name, Habitants hab) {
        this.name = name;
        this.hab = hab;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Habitants getHab() {
        return hab;
    }

    public void setHab(Habitants hab) {
        this.hab = hab;
    }
}
