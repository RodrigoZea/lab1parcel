package com.example.user.ejerciciovistas;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import static com.example.user.ejerciciovistas.AppDatabase.DATABASE_VERSION;

/**
 * Created by Rodrigo Zea on 2/28/2018.
 */

@Database(entities = {Country.class}, version = DATABASE_VERSION)
public abstract class AppDatabase extends RoomDatabase {
    public static final int DATABASE_VERSION=1;
    public static final String DATABASE_NAME="COUNTRY-DB-ROOM";

    public abstract CountryDao countryDao();

    public static AppDatabase dbInstance;

    public static AppDatabase getInstance(Context context){
        if (dbInstance == null) {
            dbInstance = Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return dbInstance;

    }


}
