package com.example.user.ejerciciovistas;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rodrigo Zea on 2/20/2018.
 */

public class modelString implements Parcelable {
    private final String st1;
    private final String st2;
    private final String st3;
    private final List<String> lcest;


    public modelString (String nombre, String prefijo, int habitantes, List<String> values){
        this.st1 = nombre;
        this.st2 = prefijo;
        this.st3 = habitantes + "";
        this.lcest = values;
    }

    public modelString (Parcel parcel){
        st1 = parcel.readString();
        st2 = parcel.readString();
        st3 = parcel.readString();

        lcest = new ArrayList<>();
        parcel.readStringList(lcest);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(st1);
        parcel.writeString(st2);
        parcel.writeString(st3);
        parcel.writeStringList(lcest);
    }

    public String getNombre(){
        return st1;
    }

    public String getPrefijo(){

        return st2;
    }

    public String getHabitantes(){

        return st3;
    }

    public static final Parcelable.Creator<modelString> CREATOR = new Creator<modelString>(){
        @Override
        public modelString createFromParcel(Parcel parcel){
            return new modelString(parcel);
        }

        @Override
        public modelString[] newArray(int i){
            return new modelString[i];
        }

    };
}
