    package com.example.user.ejerciciovistas;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

    public class MainActivity extends AppCompatActivity {

        //Variables de entorno
        ListView ListView;
        //String[] valores = new String[] {"Holanda", "España", "USA", "India"};
        List<Country> countryList = new ArrayList<>();
        ArrayAdapter adapter;

        //Todas las variables relacionadas a bases de datos
        //private CompositeDisposable cDisposable;
        private CompositeDisposable compositeDisposable;
        private CountryRepository countryRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Al iniciar
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Inicializar compDisposable
        compositeDisposable = new CompositeDisposable();

        //Se crea la listview
        ListView = (ListView) findViewById(R.id.Lista);

        //Se crea el adaptador para la lista
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, valores);
        adapter = new ArrayAdapter(this, android.R.layout.simple_expandable_list_item_1, countryList);
        registerForContextMenu(ListView);
        ListView.setAdapter(adapter);

        AppDatabase countryDatabase = AppDatabase.getInstance(this);
        countryRepo = CountryRepository.getInstance(CountryDataSource.getInstance(countryDatabase.countryDao()));

        loadDatos();

        ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id){
                Toast.makeText(getApplicationContext(), "Position"+pos, Toast.LENGTH_SHORT).show();


                /*modelString allItems = new modelString(pos, (Country) ListView.getItemAtPosition(pos), new ArrayList<Country>());
                intent.putExtra(RecieverActivity.SELECTED_ITEM, allItems);

                startActivity(intent);*/
                Country ctSend = (Country) ListView.getItemAtPosition(pos);
                String ctName = ctSend.getName();
                String ctPrefix = ctSend.getHab().getPrefix();
                int ctHabitantes = ctSend.getHab().getCant();

                Intent intent = new Intent(MainActivity.this, RecieverActivity.class);
                modelString allItems = new modelString(ctName, ctPrefix, ctHabitantes, new ArrayList<String>());
                intent.putExtra(RecieverActivity.SELECTED_ITEM, allItems);

                startActivity(intent);

            }


        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Disposable disposable = io.reactivex.Observable.create(new ObservableOnSubscribe<Object>() {
                    @Override
                    public void subscribe(ObservableEmitter<Object> e) throws Exception{
                        Country country = new Country("Holanda", new Habitants(4000000, "Holandeses"));
                        countryRepo.insertCountry(country);

                        Country country2 = new Country("Italia", new Habitants(3500000, "Italianos"));
                        countryRepo.insertCountry(country2);

                        Country country3 = new Country("Guatemala", new Habitants(1000000, "Guatemaltecos"));
                        countryRepo.insertCountry(country3);

                        e.onComplete();
                    }
                })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer() {
                            @Override
                            public void accept(Object o) throws Exception{
                                Toast.makeText(MainActivity.this, "Pais agregado", Toast.LENGTH_SHORT).show();
                            }
                        }, new Consumer<Throwable>(){
                                @Override
                                public void accept (Throwable throwable) throws Exception {
                                    Toast.makeText(MainActivity.this, ""+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                        },
                        new Action() {
                            @Override
                            public void run() throws Exception {
                                loadDatos();
                            }
                        });

            }
        });

        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteAllCountries();

            }
        });
    }

    private void loadDatos(){
        //Utiliza rxjava2 para Android
        Disposable disposable = countryRepo.getAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Country>>() {
                    @Override
                    public void accept(List<Country> countries) throws Exception {
                        onGetAllCountriesSuccess(countries);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MainActivity.this, ""+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        compositeDisposable.add(disposable);
    }

    private void onGetAllCountriesSuccess(List<Country> countries){
        countryList.clear();
        countryList.addAll(countries);
        adapter.notifyDataSetChanged();
    }

    private void deleteAllCountries(){
        Disposable disposable = io.reactivex.Observable.create(new ObservableOnSubscribe<Object>() {
            @Override
            public void subscribe(ObservableEmitter<Object> e) throws Exception{
                countryRepo.deleteAllCountries();
                e.onComplete();
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer() {
                               @Override
                               public void accept(Object o) throws Exception{

                               }
                           }, new Consumer<Throwable>(){
                               @Override
                               public void accept (Throwable throwable) throws Exception {
                                   Toast.makeText(MainActivity.this, ""+throwable.getMessage(), Toast.LENGTH_SHORT).show();
                               }
                           },
                        new Action() {
                            @Override
                            public void run() throws Exception {
                                loadDatos();
                            }
                        });

    }

    /*
    public void displayProducts(View view){
            startActivity(new Intent(this, displayProduct.class));
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
